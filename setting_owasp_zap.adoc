= OWASP ZAPのインストール後の設定
徳丸本記載のOWASP ZAPの設定方法+αを記載

- OWASP ZAP起動後、ツール/オプションで以下の様に設定する。 +

image::img/capture1.png[]

image::img/capture2.png[]

image::img/capture3.png[]

起動方法はターミナルを起動し、 `owasp-zap` コマンドを実行する。
