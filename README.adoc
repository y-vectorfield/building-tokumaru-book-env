:sectnums:

= Building Tokumaru Book Env

体系的に学ぶ安全なWebアプリケーションの作り方 第2版の演習環境構築スクリプト

== 構築方法

=== 構築用VMの用意
- LinuxのVMを用意する
- GUIが使えるデスクトップ版OSでセットアップする。
- OSはUbuntuを推奨(説明はUbuntuを前提としている)
- ユーザー名はwasbookとすることを推奨(playbookの編集が不要となる)
- このスクリプトは実機上で構築することも可能。実機で構築する場合VMを実機に読み替える
- ハイパーバイザーを利用する場合はVMware Fusion @ Mac、VMware Workstation Player @ Windowsをお薦めします。

=== rootユーザーにログイン
用意したVMにログイン後rootユーザーにログイン

----
sudo su -
----

=== Gitをインストール
構築に利用するスクリプトのリポジトリをクローンするために必要なgitをインストール

----
apt update && apt upgrade -y
apt install -y git
----

=== 構築に必要なスクリプトをクローン
ホームディレクトリ上にGitリポジトリをクローンする。

----
git clone https://gitlab.com/y-vectorfield/building-tokumaru-book-env.git
----

=== ディレクトリの移動
クローンしたリポジトリ(ディレクトリ)に移動する。

----
cd ~/building-tokumaru-book-env
----

=== 認証情報格納ファイルの作成
著者のページから必要なファイルを取得する時に必要となる認証情報を格納するファイルを作成する。

----
touch auth.txt
----

- 作成したファイルを開き、以下の様に記載する。記載内容は書籍参照
- 必ずユーザー名を入力後改行してパスワードを入力すること

----
<USER_NAME> : 書籍に記載のユーザー名を記載
<PASSWD>    : 書籍に記載のパスワードを記載
----

=== OWASP ZAPの現行バージョン番号を構築スクリプトに設定
https://www.zaproxy.org/download/[OWASP ZAPの公式ページ]を参照し、OWASP ZAPの現行バージョンの番号を調べます。
現行バージョンが分かったら、 `playbook.yml` 3行目の `zap_ver` に調べたバージョン番号を設定します。

=== 構築スクリプトの実行
構築スクリプト(`build.sh`)を実行するとAnsibleで自動的に環境構築が行われる。

----
./build.sh
----

=== rootユーザーからログアウト
セットアップ完了後はrootユーザーからログアウトします。

----
exit
----

=== FoxyProxyの設定
書籍の設定方法を参考に設定して下さい。

=== OWASP ZAPの設定
ドキュメント<<setting_owasp_zap.adoc#>>を参照して下さい。
